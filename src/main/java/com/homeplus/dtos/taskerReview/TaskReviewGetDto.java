package com.homeplus.dtos.taskerReview;

import com.homeplus.models.TaskEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
public class TaskReviewGetDto {
    private Long id;

    private TaskEntity taskEntity;

    private String review;

    private Integer rating;

    private OffsetDateTime created_time;

    private OffsetDateTime updated_time;

}
