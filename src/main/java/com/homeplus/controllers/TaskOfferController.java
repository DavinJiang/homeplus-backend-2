package com.homeplus.controllers;

import com.homeplus.dtos.taskOffer.TaskOfferGetDto;
import com.homeplus.dtos.taskOffer.TaskOfferPostDto;
import com.homeplus.dtos.taskOffer.TaskOfferPutDto;
import com.homeplus.dtos.tasker.TaskerPostDto;
import com.homeplus.dtos.tasker.TaskerPutDto;
import com.homeplus.models.TaskOfferEntity;
import com.homeplus.models.TaskOfferStatus;
import com.homeplus.services.TaskOfferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@Validated
public class TaskOfferController {

    private final TaskOfferService taskOfferService;

    @GetMapping("task-offers")
    public ResponseEntity<List<TaskOfferGetDto>> findTaskOffers(@RequestParam Long id){
        return ResponseEntity.ok(taskOfferService.getOffersByTaskId(id));
    }

    @GetMapping("pending-offers")
    public ResponseEntity<List<List<TaskOfferGetDto>>> findPendingOffers(@RequestParam Long id) {
        return ResponseEntity.ok(taskOfferService.getPendingOffers(id));
    }

    @GetMapping("task-offer")
    public ResponseEntity<TaskOfferGetDto> findTaskOffer(@RequestParam Long id) {
        return ResponseEntity.ok(taskOfferService.getOffersById(id));
    }

    @GetMapping("accepted-offers")
    public ResponseEntity<List<TaskOfferGetDto>> findAcceptedOffers(@RequestParam Long id) {
        return ResponseEntity.ok(taskOfferService.getAcceptedOffers(id));
    }

    @PostMapping("create-offer")
    public ResponseEntity<String> createTaskOffer(@Valid @RequestBody TaskOfferPostDto taskOfferPostDto) {
        taskOfferService.createTaskOffer(taskOfferPostDto);
        return ResponseEntity.ok("success");
    }

    @PutMapping("cancel-offer")
    public ResponseEntity<String> cancelOffer(@RequestParam Long id, @RequestParam String reply_msg){
        taskOfferService.updateOfferStatus(id, reply_msg, TaskOfferStatus.CANCELED);
        return ResponseEntity.ok("your task offer has been canceled");
    }

    @PutMapping("accept-offer")
    public ResponseEntity<String> acceptOffer(@RequestParam Long id, @RequestParam String reply_msg){
        taskOfferService.updateOfferStatus(id, reply_msg, TaskOfferStatus.ACCEPTED);
        return ResponseEntity.ok("accepted the offer");
    }

    @PutMapping("reject-offer")
    public ResponseEntity<String> rejectOffer(@RequestParam Long id, @RequestParam String reply_msg){
        taskOfferService.updateOfferStatus(id, reply_msg, TaskOfferStatus.REJECTED);
        return ResponseEntity.ok("rejected the offer");
    }
}
