package com.homeplus.utility.mapper;

import com.homeplus.dtos.comment.CommentGetDto;
import com.homeplus.dtos.comment.CommentPostDto;
import com.homeplus.dtos.comment.CommentPutDto;
import com.homeplus.models.CommentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentMapper {
    CommentEntity postDtoToEntity(CommentPostDto commentPostDto);

    CommentEntity putDtoToEntity(CommentPutDto commentPutDto);

    CommentGetDto fromEntity(CommentEntity commentEntity);
}
