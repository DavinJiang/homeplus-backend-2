package com.homeplus.models;

public enum Status {
    UNVERIFIED,
    ACTIVATED,
    CANCELLED
}
