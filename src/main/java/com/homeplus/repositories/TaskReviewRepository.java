package com.homeplus.repositories;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskReviewEntity;
import com.homeplus.models.TaskerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@EnableJpaRepositories
public interface TaskReviewRepository extends JpaRepository<TaskReviewEntity, Long> {

    @Query("select t from TaskReviewEntity as t where t.taskEntity = :taskEntity")
    TaskReviewEntity getReviewsByTask(@Param("taskEntity") TaskEntity task);
}
